"use strict";

let number = Number(prompt("Please input a number"))

console.log(`The number you provided is ${number}`)

for(let i = number; i >0; i--) {

    if(i % 10 === 0){
        console.log("The number is divisable by 10. skipping the number")
        continue
    }
    if(i % 5 === 0) {
        console.log(i)
        continue
    }
    if(i <= 50) {
        console.log("The current value is at 50. Terminating Loop")
        break
    }
 
}

let str = "supercalifragilisticexpialidocious"

let consonants = ""

for (let i = 0; i < str.length; i++) {

  if(str[i] == "a" || str[i] == "e" || str[i] == "i" || str[i] == "o" || str[i] == "u"){
    continue
  }
  consonants = consonants + str[i]
}

console.log(str)
console.log(consonants)
